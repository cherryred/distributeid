package main

import (
	"flag"
	"fmt"

	"gitee.com/cherryred/distributeid/internal/config"
	"gitee.com/cherryred/distributeid/internal/server"
	"gitee.com/cherryred/distributeid/internal/svc"
	"gitee.com/cherryred/distributeid/pb/distributeid"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/core/service"
	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var configFile = flag.String("f", "etc/distributeid.yaml", "the config file")

func main() {
	flag.Parse()

	var c config.Config
	conf.MustLoad(*configFile, &c)
	ctx := svc.NewServiceContext(c)

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		distributeid.RegisterDistributeIdServer(grpcServer, server.NewDistributeIdServer(ctx))

		if c.Mode == service.DevMode || c.Mode == service.TestMode {
			reflection.Register(grpcServer)
		}
	})
	defer s.Stop()

	fmt.Printf("Starting rpc server at %s...\n", c.ListenOn)
	s.Start()
}
