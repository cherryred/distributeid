package snowflake

import (
	"fmt"
	"sync"
	"time"
)

// Snowflake 结构体
type Snowflake struct {
	mutex          sync.Mutex // 互斥锁
	epoch          int64      // 起始时间戳（毫秒）
	nodeIDBits     uint       // 节点ID所占的位数
	sequenceBits   uint       // 序列号所占的位数
	nodeIDShift    uint       // 节点左移位数
	timestampShift uint       // 时间左移位数
	sequenceMask   int64      // 序列号有效位
	nodeID         int64      // 节点ID
	lastTimestamp  int64      // 上一次生成ID的时间戳
	sequence       int64      // 序列号
}

// NewSnowflake 函数用于创建一个Snowflake实例
func NewSnowflake(nodeID int64) *Snowflake {
	s := new(Snowflake)
	s.epoch = 1697354687979
	s.nodeIDBits = 10
	s.sequenceBits = 12
	s.nodeIDShift = s.sequenceBits
	s.timestampShift = s.nodeIDBits + s.nodeIDShift
	s.sequenceMask = -1 ^ (-1 << s.sequenceBits)
	s.nodeID = nodeID
	s.lastTimestamp = -1
	s.sequence = 0
	s.mutex = sync.Mutex{}
	return s
}

// GenerateId 方法用于生成一个全局唯一的ID
func (s *Snowflake) GenerateId() int64 {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	currentTimestamp := time.Now().UnixNano() / 1000000
	if s.lastTimestamp == currentTimestamp {
		s.sequence = (s.sequence + 1) & s.sequenceMask
		if s.sequence == 0 {
			for currentTimestamp <= s.lastTimestamp {
				currentTimestamp = time.Now().UnixNano() / 1000000
			}
		}
	} else {
		s.sequence = 0
	}

	s.lastTimestamp = currentTimestamp

	id := (currentTimestamp-s.epoch)<<s.timestampShift | (s.nodeID << s.nodeIDShift) | s.sequence
	return id
}

func exampleGenerateId() {
	// 创建两个Snowflake实例，对应两个不同的节点
	s1 := NewSnowflake(1)
	s2 := NewSnowflake(2)

	// 生成10个ID，并输出
	for i := 0; i < 10; i++ {
		id1 := s1.GenerateId()
		id2 := s2.GenerateId()
		fmt.Println("Node 1 ID:", id1)
		fmt.Println("Node 2 ID:", id2)
		fmt.Println()
	}
}
