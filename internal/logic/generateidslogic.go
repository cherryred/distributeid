package logic

import (
	"context"

	"gitee.com/cherryred/distributeid/internal/svc"
	"gitee.com/cherryred/distributeid/pb/distributeid"

	"github.com/zeromicro/go-zero/core/logx"
)

type GenerateIdsLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGenerateIdsLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GenerateIdsLogic {
	return &GenerateIdsLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GenerateIdsLogic) GenerateIds(in *distributeid.GetDistributeIdReq) (*distributeid.GetDistributeIdRsp, error) {
	ret := &distributeid.GetDistributeIdRsp{}
	for i := int32(0); i < in.Count; i++ {
		id := l.svcCtx.Distributor.GenerateId()
		ret.Ids = append(ret.Ids, id)
	}
	return ret, nil
}
