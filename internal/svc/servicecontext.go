package svc

import (
	"gitee.com/cherryred/distributeid/internal/common/snowflake"
	"gitee.com/cherryred/distributeid/internal/config"
	"github.com/zeromicro/go-zero/core/logx"
)

type ServiceContext struct {
	Config      config.Config
	Distributor *snowflake.Snowflake
}

func NewServiceContext(c config.Config) *ServiceContext {
	logx.SetLevel(logx.DebugLevel)
	return &ServiceContext{
		Config:      c,
		Distributor: snowflake.NewSnowflake(c.NodeId),
	}
}
