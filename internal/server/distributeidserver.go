// Code generated by goctl. DO NOT EDIT.
// Source: distributeid.proto

package server

import (
	"context"

	"gitee.com/cherryred/distributeid/internal/logic"
	"gitee.com/cherryred/distributeid/internal/svc"
	"gitee.com/cherryred/distributeid/pb/distributeid"
)

type DistributeIdServer struct {
	svcCtx *svc.ServiceContext
	distributeid.UnimplementedDistributeIdServer
}

func NewDistributeIdServer(svcCtx *svc.ServiceContext) *DistributeIdServer {
	return &DistributeIdServer{
		svcCtx: svcCtx,
	}
}

func (s *DistributeIdServer) GenerateIds(ctx context.Context, in *distributeid.GetDistributeIdReq) (*distributeid.GetDistributeIdRsp, error) {
	l := logic.NewGenerateIdsLogic(ctx, s.svcCtx)
	return l.GenerateIds(in)
}
